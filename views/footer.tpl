      <footer class="blog-footer">
        <p>
            <span style="text-decoration:none">
                <a href="/feed">Atom Feed <img src="/static/img/feed.svg" alt="feed" height="10" width="10"></a>
            </span>
        </p>

        <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">
            <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/80x15.png">
        </a>
        <p class="text-muted small">
          This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
          Commons Attribution-NoDerivatives 4.0 International License</a>.
        </p>
      </footer>

    </body>
</html>
