# Learn You Some Algebras for Glorious Good!

This is a math book. We aim to take a logically rigorous, yet informal approach
to math.

The title (and content) is inspired by Miran Lipovača's
[Learn You a Haskell for Great Good!][lyah]. LYAH is unfortunately a bit out of
date, so I don't recommend it for Haskell. I instead recommend
[bitemyapp's guide][bmag].

[lyah]: http://learnyouahaskell.com/
[bmag]: https://github.com/bitemyapp/learnhaskell
[webchat]: https://webchat.freenode.net/?channels=%23lysa

I have observed that most math (and science) books nowadays seem to take an
approach wherein it's more important to keep an academic tone than it is for the
reader to understand the material, and, more importantly, enjoy reading the
book. We take the opposite approach. We want to create a book that is fun to
read, easy to understand, while still addressing the more advanced concepts in
math.

If you like chatting on IRC, come see us in `#lysa` on FreeNode. If you don't
know what IRC is, or you don't have a client set up, you can connect through
[FreeNode's webchat][webchat]. You can email me (pharpend) at
`peter@harpending.org`. I'm `pharpend` in the channel.

If you have any questions about LYSA (or math), feel free to ask in the channel,
or in the issue tracker.

We'll probably set up a blog pretty soon.

The maintainer of this site is Nick Chambers. He can be reached at
`DTSCode@gmail.com`.

# License

LYSA is licensed under the [GNU Free Documentation License][ccsa]. This means
many things, but here's the gist of it:

* You are free to read this book, redistribute it, change it, sell it,
what-have-you. There are a minimum of strings attached.
* The first string - you have to give us credit. You can't claim that you wrote
this book all on your own.
* The second thing - you are welcome to make changes, but, if you publish your
  changes, you must publish your changes under the same license. This ensures
  that we can then integrate your changes back into the main work.

If you want to know the details, read the [legal code][ccsa].

[ccsa]: http://www.gnu.org/licenses/fdl.html

The content of this site is licensed under the
[Creative Commons Attribution-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nd/4.0/legalcode).