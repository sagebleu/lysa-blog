# Frequently Asked Questions

1.  **Why is LYSA hosted on GitLab, and not GitHub?**

    GitHub is proprietary, GitLab is
    [free](https://gnu.org/philosophy/free-sw.html). We believe in freedom, and
    thus, it betrays our goals to rely on a proprietary platform.

2.  **Why is LYSA licensed under the GNU Free Documentation License, and not the
    Creative Commons ShareAlike license?**

    The FDL is designed specifically for textbooks and manuals, whereas the
    CC-SA license is designed for any creative work. Thus, the wording in the
    FDL, is much more specific to our situation, and therefore less
    ambiguous. 

    For instance, the FDL requires that the document be made available in source
    form. This is hugely important for a textbook, because otherwise you
    wouldn't be able to edit it. The CC-SA license has no such requirement.

3.  **Do you guys have a blog or something?**

    A (wo)?man named Enzo Calamia is working on developing a blog for
    http://learnyou.org . I don't believe it's finished yet. However, at the
    moment, the de-facto "blog" and "forum" is our subreddit:
    https://lysa.reddit.com/ . You can ask questions there, and someone will
    hopefully answer them.

4.  **I found an error, what do I do?**

    If you know how to fix it, it would be best if you would clone the git repo,
    fix the issue, and then send a merge request. If you don't want to do this,
    the next best thing would be to
    [open a bug](https://gitlab.com/lysa/lysa/issues/new).

    Unfortunately, opening a bug requires a GitLab account. GitLab accounts are
    free (in both senses of the word). If you don't want to make a GitLab
    account for whatever reason, you can email `peter@harpending.org` with the
    issue.

5.  **Why is LYSA only available as a PDF? Can I get it in some other format?**

    In theory, yes. However, the only tool that can do this is
    [Pandoc](http://johnmacfarlane.net/pandoc/). Unfortunately, Pandoc is really
    buggy, so we can't convert it yet.

6.  **How do I get in touch with you?**

    You could email me, `peter@harpending.org`. We have an IRC channel, `#lysa`
    on FreeNode. If you want a forum-type experience, we have a
    [subreddit](https://lysa.reddit.com/).


    